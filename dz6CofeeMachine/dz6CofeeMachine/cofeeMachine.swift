//
//  coffe_machine.swift
//  dz6_cofee_machine
//
//  Created by Andrey Bakanov on 3/21/19.
//  Copyright © 2019 Andrey Bakanov. All rights reserved.
//

import UIKit


class cofeeMachine: NSObject {
    
    var water = 0
    let maxWater = 500
    
    var beans = 0
    let maxBeans = 100
    
    var milk = 0
    let maxMilk = 200
    
    
    func makeCofee() -> String  {
        if water >= 100 && beans >= 20{
            water = water - 100
            beans = beans - 20
            
            }
        else  if water <= 100 {return "add water"}
            
        else  if beans <= 20 {return "add beans"}
        
        return "Your coffe"
    }
    
    func makeCapuchino() -> String  {
        if water >= 100 && beans >= 20 && milk >= 50 {
            water = water - 100
            beans = beans - 20
            milk = milk - 50
        }
        else  if water <= 100 {return "add water"}
        else  if beans <= 20 {return "add beans"}
        else  if milk <= 20 {return "add milk"}
        return "Your capuchino"
    }
    
    func addWater(amountWater: Int) -> String {
        if water + amountWater <= maxWater {
            water += amountWater
        
        }
        else   {
            return "too much water amount"
        }
       return "you added water"
    }
    
    func addBeans(amountBean: Int) -> String{
        if beans + amountBean <= maxBeans {
            beans += amountBean
            return "you added beans"
        }
        else if  beans + amountBean >= maxBeans{
             return ("too much beans amount")
        }
        return "you added beans"
    }
    
    func addMilk(amountMilk: Int) -> String{
        if milk + amountMilk <= maxMilk {
            milk += amountMilk
        }
        else {
            return ("too much milk amount")
        }
        return "you added milk"
    }
    
}



